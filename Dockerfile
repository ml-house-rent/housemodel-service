FROM python:3.7

# Install requirements
RUN pip install --upgrade pip && pip install pipenv

# Make app structure
RUN mkdir /app/
ADD . /app/
RUN cd /app && rm -rf .git/ && pipenv install --system
WORKDIR /app/

# Expose and run
EXPOSE 8080
CMD ["./run.sh"]
