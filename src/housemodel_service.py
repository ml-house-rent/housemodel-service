from housemodel.model import HouseRentModelBuilder

model = HouseRentModelBuilder().get_model()
model.load_model()
