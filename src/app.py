from flask import Flask, request, jsonify
from housemodel_service import model


app = Flask(__name__)

@app.route('/')
def url_doc():
    urls = {'predict': '/predict'}
    response = jsonify(urls)
    response.status_code = 200
    return response

@app.route('/predict')
def hello_world():
    m2_list = request.args.get('m2')
    if not m2_list:
        err = {'error': 'Need the m2 parameter'}
        response = jsonify(err)
        response.status_code = 400
        return response
    m2_list = m2_list.split(',')
    try:
        m2_list = list(map(int, m2_list))
    except ValueError as e:
        err = {'error': 'Some value is not an integer'}
        print(e)
        response = jsonify(err)
        response.status_code = 400
        return response
    predictions = model.predict(m2_list)
    results = {}
    i = 0
    for m2 in m2_list:
        results[m2] = int(predictions[i])
        i += 1
    response = jsonify(results)
    response.status_code = 200
    return response


if __name__ == '__main__':
    # WARNING: Only for development!
    app.run(port=8080)
