#!/bin/bash


RED='\033[0;31m'
NOCOLOR='\033[0m'
IMAGE_NAME="housemodel"
PORT_BIND=8080

if [[ $1 == "-b" || $1 == "--build" ]]; then
    docker image build -t ${IMAGE_NAME} .
    err_code=$?
    if [[ ${err_code} -ne 0 ]]; then
        echo -e "${RED}Something went wrong when building container (error: ${err_code})${NOCOLOR}" >&2
        exit ${err_code}
    fi
    shift
fi

if [[ $# -gt 0 ]]; then
    PORT_BIND=$1
    shift
fi

docker container run -it --rm -p ${PORT_BIND}:8080 ${IMAGE_NAME}
