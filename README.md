# House Rent Predictions service

## How to run (Docker)
Execute the following line in your prompt to build and run the service:
```bash
./run_docker.sh -b 8080
```

## How to use it
You can perform requests onnce the service is up to perform predictions:
```bash
curl --silent http://localhost:8080/predict\?m2\=55,90 | python -m json.tool
```
